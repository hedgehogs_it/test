﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Test3.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult LogIn()
        {
            return Redirect("https://oauth.vk.com/authorize?client_id=5703072&scope=8&redirect_uri=http://kobzar-001-site1.htempurl.com/Account/GetVkResponse");
        }

        [Route("getVkResponse")]
        public ActionResult GetVkResponse(string code)
        {
            if (code != null)
            {
                var req = WebRequest.Create("https://oauth.vk.com/access_token?client_id=5703072&client_secret=P5jEGgX6ocSbwVZI8qMz&redirect_uri=http://kobzar-001-site1.htempurl.com/Account/GetVkResponse&code=" + code);
                var resp = req.GetResponse();
                using (StreamReader stream = new StreamReader(resp.GetResponseStream()))
                {
                    var response = stream.ReadToEnd();
                    string cookie = Models.Auth.Authentication.Encrypt(response);
                    HttpCookie c = new HttpCookie(ConfigurationManager.AppSettings["CookieName"], cookie);
                    Response.Cookies.Add(c);
                }
                return RedirectToAction("Renew", "Account");
            }
            return HttpNotFound();
        }

        public ActionResult Renew()
        {
            Web.Models.AccountModel.RenewUserInfo();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {
            Response.Cookies.Remove(ConfigurationManager.AppSettings["CookieName"]);
            return RedirectToAction("Index", "Home");
        }

    }
}