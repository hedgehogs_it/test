﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Database.Contexts;
using Database.Models;

namespace Test3.Controllers
{
    public partial class AudioController : Controller
    {
        // GET: Ratings
        public JsonResult AddRating(Rating rating)
        {
            using (AudioContext db = new AudioContext())
            {
                rating.UserId = Models.Auth.Authentication.UserId;
                db.Ratings.Add(rating);
                db.SaveChanges();
            }
            return Json("");
        }
    }
}