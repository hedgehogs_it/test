﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Api.Requests;
using Api.Models;
using Database.Contexts;

namespace Test3.Controllers
{
    public partial class AudioController : Controller
    {
        // GET: Audio
        public ActionResult Get()
        {
            var audio = GetAudio.GetUserAudio(Models.Auth.Authentication.ResponseToken);
            return View("~/Views/Audio/_Audio.cshtml", audio);
        }

        public ActionResult Search(string titleQuery, string artistQuery, Ganres? ganre)
        {
            var audio = GetAudio.Search(titleQuery, artistQuery, ganre, Models.Auth.Authentication.ResponseToken);
            return View("~/Views/Audio/_Audio.cshtml", audio);
        }

        public JsonResult GetAudioData(int ownerId, int songId)
        {
            using (AudioContext db = new AudioContext())
            {
                var messages = db.Messages.Where(m => m.OwnerId == ownerId && m.SongId == songId).ToList();
                var result = messages.Select(m => new { message = m.Message, photo = m.User.Photo, time = m.Time }).OrderBy(m=>m.time).ToList();
                var ratings = db.Ratings.Where(r => r.OwnerId == ownerId && r.SongId == songId).ToList();
                var songRating = 0;
                if (ratings.Count > 0)
                {
                    var sum = ratings.Sum(r => r.Rate);
                    songRating = sum / ratings.Count;
                }
                var userRate = 0;
                if(ratings.FirstOrDefault(r=>r.UserId == Models.Auth.Authentication.UserId)!=null)
                {
                    userRate = ratings.FirstOrDefault(r => r.UserId == Models.Auth.Authentication.UserId).Rate;
                }
                var lyrics = Api.Requests.GetAudio.GetLirycs(songId, ownerId, Models.Auth.Authentication.ResponseToken);
                var text = lyrics.LyricsText.Replace("/n", "<br />");
                return Json(new { messages = result,songRate = songRating, userRate = userRate,lyrics= text }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}