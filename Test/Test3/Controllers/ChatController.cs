﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Database.Contexts;
using Database.Models;

namespace Test3.Controllers
{
    public partial class AudioController : Controller
    {
        [HttpPost]
        public JsonResult AddMessage(int ownerId, int songId, int time, string message)
        {
            using (AudioContext db = new AudioContext())
            {
                ChatMessage m = new ChatMessage()
                {
                    Message = message,
                    OwnerId = ownerId,
                    SongId = songId,
                    Time = time,
                    UserId = Models.Auth.Authentication.UserId,
                    User_Id = Models.Auth.Authentication.CurrentUser.Id
                };
                db.Messages.Add(m);
                db.SaveChanges();
            }
            return Json("");
        }

        public JsonResult GetMessages(int ownerId, int songId)
        {
            using (AudioContext db = new AudioContext())
            {
                var messages = db.Messages.Where(m => m.OwnerId == ownerId && m.SongId == songId).ToList();
                var result = messages.Select(m => new { message = m.Message, photo = m.User.Photo, time = m.Time }).ToList();
                return Json(new { messages = result }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}