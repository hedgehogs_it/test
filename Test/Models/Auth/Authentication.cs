﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.Security.Cryptography;
using System.IO;

namespace Models.Auth
{
    public class Authentication
    {

        private static string AuthKey = ConfigurationManager.AppSettings["AuthKey"];
        private static string AuthIV = ConfigurationManager.AppSettings["AuthIV"];

        public static bool IsLocal
        {
            get
            {
               return HttpContext.Current.Request.IsLocal;
            }
        }

        public static bool IsAuth
        {
            get
            {
                return CookieValue != null;
            }
        }

        public static string CookieValue
        {
            get
            {
                string _cookieValue = null;
                try
                {
                    if (!IsLocal)
                    {
                        var httpCtx = HttpContext.Current;
                        if (httpCtx.Request != null && httpCtx.Request.Cookies != null)
                        {
                            var cookie = httpCtx.Request.Cookies[ConfigurationManager.AppSettings["CookieName"]];
                            if (cookie != null && cookie.Value != null && cookie.Value != string.Empty && !string.IsNullOrWhiteSpace(cookie.Value))
                            {
                                _cookieValue = Decrypt(cookie.Value);
                            }
                        }
                    }
                    else
                    {
                        _cookieValue = Decrypt("h1axnGXFntN1DC0G/edBr2JVFDHcIumJTtIaUriy3LcMgPLOVu/SqtRGQMnp5hFaKT1+1DTJQqD5HvY+lTQMvUGKgZbzyHaORPP3C7XWY6HxclanJBs8GcDwnEt0Fdp0fShNEXs4l2gt798DTvzcVBD6MVdO+806+soJ1hnafQQ/C2xjUIBA28WjdTYXtyO9");
                    }
                }
                catch { }
                return _cookieValue;
            }
        }

        public static string ResponseToken
        {
            get
            {
                if (CookieValue == null)
                    return null;
                string[] values = CookieValue.Split(new char[] { ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i + 1 < values.Count(); i += 2)
                {
                    var val = values[i].Replace("\"", "").Replace("{", "").Replace("}", "");
                    if (val == "access_token")
                        return values[i + 1].Replace("\"", "").Replace("{", "").Replace("}", "");

                }
                return null;
            }
        }

        public static string UserId
        {
            get
            {
                if (CookieValue == null)
                    return null;
                string[] values = CookieValue.Split(new char[] { ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i + 1 < values.Count(); i += 2)
                {
                    var val = values[i].Replace("\"", "").Replace("{", "").Replace("}", "");
                    if (val == "user_id")
                        return values[i + 1].Replace("\"", "").Replace("{", "").Replace("}", "");

                }
                return null;
            }
        }

        public static Database.Models.User CurrentUser
        {
            get
            {
                using (Database.Contexts.AudioContext db = new Database.Contexts.AudioContext())
                {
                    return db.Users.FirstOrDefault(u => u.UserId == UserId);
                }
            }
        }

        public static string Encrypt(string plainText)
        {
            byte[] Key = Convert.FromBase64String(AuthKey);
            byte[] IV = Convert.FromBase64String(AuthIV);
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return Convert.ToBase64String(encrypted);

        }

        private static string Decrypt(string text)
        {
            byte[] Key = Convert.FromBase64String(AuthKey);
            byte[] IV = Convert.FromBase64String(AuthIV);
            byte[] cipherText = Convert.FromBase64String(text);
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

    }
}
