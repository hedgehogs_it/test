﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models
{
    public class Audio
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("owner_id")]
        public int OwnerId { get; set; }

        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("date")]
        public long Date { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("lyrics_id")]
        public int LyricsId { get; set; }

        [JsonProperty("genre_id")]
        public int GanreId { get; set; }
    }

    class AudioResponse
    {
        [JsonProperty("items")]
        public List<Audio> Response { get; set; }
    }

    class SearchResponse
    {
        [JsonProperty("byartist")]
        public List<Audio> ByArtist { get; set; }

        [JsonProperty("title")]
        public List<Audio> ByTitle { get; set; }

    }
}