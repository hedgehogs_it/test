﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models
{
    public class Lyrics
    {
        [JsonProperty("text")]
        public string LyricsText { get; set; }
    }
}