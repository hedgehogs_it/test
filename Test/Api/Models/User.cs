﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models
{
    public class User
    {
        [JsonProperty("id")]
        public string UserId { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("photo_50")]
        public string Photo { get; set; }
    }

    class UserRequest
    {
        [JsonProperty("response")]
        public List<User> Response { get; set; }
    }
}
