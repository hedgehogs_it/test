﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models
{
    class Response<T> where T : new()
    {
        [JsonProperty("response")]
        public T Value { get; set; }

        public Response()
        {
            Value = new T();
        }
    }
}
