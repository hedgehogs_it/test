﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;

namespace Api.Requests
{
    public class GetAudio
    {
        public static List<Audio> GetUserAudio(string accessToken)
        {
            var result = JsonWebRequest<Response<AudioResponse>>.CreateRequest("https://api.vk.com/method/audio.get?access_token=" + accessToken + "&v=5.6");
            return result.Value.Response.Take(50).ToList();
        }

        public static List<Audio> Search(string titleQuery, string artistQuery, Ganres? ganre, string accessToken)
        {
            List<Audio> result = null;
            if (string.IsNullOrEmpty(artistQuery) && string.IsNullOrWhiteSpace(artistQuery) && string.IsNullOrEmpty(titleQuery) && string.IsNullOrWhiteSpace(titleQuery))
            {
                result = GetUserAudio(accessToken);
            }
            else
            {
                string byTitle = "var b = ";
                if (!string.IsNullOrEmpty(titleQuery) && !string.IsNullOrWhiteSpace(titleQuery))
                {
                    byTitle += "API.audio.search({\"q\":\"" + titleQuery + "\",\"count\":300});";
                }
                else
                {
                    byTitle += "{\"items\":[]};";
                }
                var byArtist = "var a = ";
                if (!string.IsNullOrEmpty(artistQuery) && !string.IsNullOrWhiteSpace(artistQuery))
                {
                    byArtist += "API.audio.search({\"q\":\"" + artistQuery + "\",\"performer_only\":\"1\",\"count\":300});";
                }
                else
                {
                    byArtist += "{\"items\":[]};";
                }

                var code = byArtist + byTitle + executeCode;
                code = code.ToString();

                var requestResult = JsonWebRequest<Response<SearchResponse>>.CreateRequest("https://api.vk.com/method/execute?code=" + code + "&access_token=" + accessToken + "&v=5.6");

                if (!string.IsNullOrEmpty(artistQuery) && !string.IsNullOrWhiteSpace(artistQuery) && !string.IsNullOrEmpty(titleQuery) && !string.IsNullOrWhiteSpace(titleQuery))
                {
                    result = requestResult.Value.ByTitle.Where(a => requestResult.Value.ByArtist.Any(ta => ta.Id == a.Id)).ToList();
                }
                else if (!string.IsNullOrEmpty(artistQuery) && !string.IsNullOrWhiteSpace(artistQuery))
                {
                    result = requestResult.Value.ByArtist;
                }
                else
                {
                    result = requestResult.Value.ByTitle;
                }
            }

            if (ganre.HasValue)
            {
                result = result.Where(a => a.GanreId == (int)ganre).ToList();
            }
            return result.ToList();

        }

        public static Lyrics GetLirycs(string lirycsId, string accessToken)
        {
            var requestResult = JsonWebRequest<Response<Lyrics>>.CreateRequest("https://api.vk.com/method/audio.getLyrics?lyrics_id=" + lirycsId + "&access_token=" + accessToken + "&v=5.6");
            return requestResult.Value;
        }

        public static Lyrics GetLirycs(int songId, int ownerId, string accessToken)
        { 
            var ownerSongId = ownerId +"_"+ songId;
            var code = ("var a = API.audio.getById({\"audios\":\""+ ownerSongId+"\"});var b = API.audio.getLyrics({ \"lyrics_id\":a[0][\"lyrics_id\"]});return b; ").ToString();
            var requestResult = JsonWebRequest<Response<Lyrics>>.CreateRequest("https://api.vk.com/method/execute?code=" + code + "&access_token=" + accessToken + "&v=5.6");
            return requestResult.Value;
        }

        private static string executeCode =
        "var c =" +
        "{\"byartist\":a[\"items\"]," +
        "\"title\":b[\"items\"]};" +
        "return c;";

    }
}

public enum Ganres
{
    Rock = 1,
    Pop = 2,
    HipHop = 3,
    EasyListening = 4,
    House = 5,
    Instrumental = 6,
    Metal = 7,
    Alternative = 21,
    Dubstep = 8,
    Jazz = 1001,
    Drum = 10,
    Trance = 11,
    Chanson = 12,
    Ethnic = 13,
    Acoustic = 14,
    Reggae = 15,
    Classical = 16,
    Indie = 17,
    Speech = 19,
    Electropop = 22,
    Other = 18
}
