﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Requests
{
    public class GetUser
    {
        public static User Get(string userId, string accessToken)
        {
            var result = JsonWebRequest<UserRequest>.CreateRequest("https://api.vk.com/method/users.get?user_ids="+userId+"&fields=photo_50&access_token=" + accessToken + "&v=5.6");
            return result.Response.FirstOrDefault();
        }
    }
}
