﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace Api
{
    /// <summary>
    /// This class creates web requests that parse a json response and 
    /// return the deserialized value.
    /// </summary>
    /// <typeparam name="T">The type to serialize the response to</typeparam>
    public class JsonWebRequest<T> where T : new()
    {
        /// <summary>
        /// Creates a synchronous request to the given url
        /// </summary>
        /// <param name="url">The url to make the request to</param>
        /// <returns>The deserialized response object</returns>
        public static T CreateRequest(string url)
        {
            var result = new T();
            var getRequest = (HttpWebRequest)WebRequest.Create(url);
            using (var getResponse = getRequest.GetResponse())
            using (var reader = new System.IO.StreamReader(getResponse.GetResponseStream()))
            {
                var responseText = reader.ReadToEnd();
                try
                {
                    result = JsonConvert.DeserializeObject<T>(responseText);
                //throw new Exception();
                }
                catch(Exception e)
                {
                    throw new Exception(responseText+result.ToString());
                }
            }
            return result;
        }
    }
}