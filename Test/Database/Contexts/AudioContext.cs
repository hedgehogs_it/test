﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Models;
using System.Data.Entity;

namespace Database.Contexts
{
    public class AudioContext : DbContext
    {
        public AudioContext() :base("AudioContext")
        {

        }

        public DbSet<Rating> Ratings { get; set; }
        public DbSet<ChatMessage> Messages { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
