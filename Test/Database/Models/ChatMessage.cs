﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Models
{
    public class ChatMessage
    {
        public int Id { get; set; }

        public int User_Id { get; set; }

        public string UserId { get; set; }

        public int OwnerId { get; set; }

        public int SongId { get; set; }

        public int Time { get; set; }

        public string Message { get; set; }

        [ForeignKey("User_Id")]
        public virtual User User { get; set; }
    }
}
