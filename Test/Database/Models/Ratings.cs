﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Models
{
    public class Rating
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public int OwnerId { get; set; }

        public int SongId { get; set; }

        public short Rate { get; set; }
    }
}
